import 'package:flutter/material.dart';

import 'store_stack_photo.dart';

class BookmarkStore extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new StoreStackPhoto(
      height: 100.0,
      start: new NameAndRatings(
        name: 'Klevis burgerhouse',
        rating: 5.0,
        reviewsCount: 65535,
      ),
      end: new Column(
        mainAxisAlignment: MainAxisAlignment.end,
        crossAxisAlignment: CrossAxisAlignment.end,
        children: <Widget>[
          new Text(
            'Burger, Klevis',
            style: new TextStyle(color: Colors.white, fontSize: 11.0,),
          ),
          new Text(
            "\$\$\$",
            style: new TextStyle(color: Colors.white, fontSize: 11.0,),
          ),
        ],
      ),
    );
  }
}
