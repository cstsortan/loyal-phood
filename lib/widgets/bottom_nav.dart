import 'package:flutter/material.dart';

import 'bottom_navigation_item.dart';

class BottomNav extends StatelessWidget {
  final int pageNum;
  final ValueChanged<int> pageChosen;
  BottomNav(this.pageNum, this.pageChosen);
  @override
  Widget build(BuildContext context) {
    return new Material(
      elevation: 8.0,
      child: new Container(
        color: new Color(0xFFFFFFFF),
        height: 50.0,
        child: new Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            new Container(
              width: MediaQuery.of(context).size.width / 2 - 35,
              child: new Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  new BottomNavigationItem(
                    assetIconName: 'nearby_icon_selected',
                    icon: Icons.location_on,
                    name: 'Nearby',
                    checked: pageNum == 0,
                    onTap: () {
                      pageChosen(0);
                    },
                  ),
                  new BottomNavigationItem(
                    assetIconName: 'check_in_icon',
                    name: 'Check in',
                    checked: pageNum == 1,
                    icon: Icons.beenhere,
                    onTap: () {
                      pageChosen(1);
                    },
                  ),
                ],
              ),
            ),
            new Container(
              width: MediaQuery.of(context).size.width / 2 - 35,
              child: new Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  new BottomNavigationItem(
                    assetIconName: 'bookmarks_icon',
                    name: 'Bookmarks',
                    checked: pageNum == 2,
                    icon: Icons.bookmark,
                    onTap: () {
                      pageChosen(2);
                    },
                  ),
                  new BottomNavigationItem(
                    assetIconName: 'profile_icon',
                    name: 'Profile',
                    checked: pageNum == 3,
                    icon: Icons.face,
                    onTap: () {
                      pageChosen(3);
                    },
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
