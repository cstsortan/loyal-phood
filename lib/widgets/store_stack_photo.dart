import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

import 'rating.dart';

class StoreStackPhoto extends StatelessWidget {
  final String photo;
  final double height;
  final Widget start;
  final Widget end;
  final double opacity;
  StoreStackPhoto({
    this.photo =
      'https://lh3.googleusercontent.com/DIW1QBTxIj4K4xqtwz9qInWKypnIrjgmof6NIbhWpLVGsFSgPsebdMPGy7_UDMy8jFA=w300-rw',
    this.height,
    this.start,
    this.end,

    this.opacity = 0.75,
  });

  @override
  Widget build(BuildContext context) {
    return new Container(
      height: height, // 100.0 - animation.value,
      child: new Stack(
        children: <Widget>[
          new Opacity(
            opacity: opacity,
            child: new Image.network(
              photo,
              fit: BoxFit.fitWidth,
              width: MediaQuery.of(context).size.width,
            ),
          ),
          new Container(
            padding: const EdgeInsets.only(
              left: 10.0,
              bottom: 10.0,
              right: 10.0,
            ),
            child: new Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                new Container(
                  child: start,
                ),
                new Container(
                  child: end,
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}

class NameAndRatings extends StatelessWidget {
  final String name;
  final double rating;
  final int reviewsCount;
  final double scale;

  NameAndRatings({
    @required this.name,
    @required this.rating,
    @required this.reviewsCount,
    this.scale = 1.0,
  })
      : assert(scale <= 1.0 && scale >= 0.0);

  @override
  Widget build(BuildContext context) {
    return new Column(
      mainAxisAlignment: MainAxisAlignment.end,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        new Material(
          elevation: 6.0,
          color: new Color(0x00000000),
          child: new Text(
            name,
            style: new TextStyle(
              color: Colors.white,
              fontSize: 20.0 * scale,
            ),
          ),
        ),
        new Row(
          children: <Widget>[
            new Rating(
              rating: rating,
              scale: scale,
            ),
            new Container(
              margin: const EdgeInsets.only(left: 10.0),
              child: new Material(
                elevation: 2.0,
                color: new Color(0x00000000),
                child: new Text(
                  '$reviewsCount reviews',
                  style: new TextStyle(
                    color: new Color(0xFFFFFFFF),
                    fontSize: 10.0 * scale,
                  ),
                ),
              ),
            ),
          ],
        ),
      ],
    );
  }
}
