import 'package:flutter/material.dart';

class TypesListCard extends StatelessWidget {
  final int selectedType;
  final ValueChanged<int> onTypeChosen;
  TypesListCard({
    this.selectedType = 1,
    this.onTypeChosen,
  });
  @override
  Widget build(BuildContext context) {
    return new Material(
      child: new Container(
        height: 60.0,
        color: new Color(0xFFFFFFFF),
        child: new Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            new StoreTypeSelection(
              asset: 'assets/cafe_icon.png',
              selected: selectedType==0,
              onTap: () {onTypeChosen(0);},
            ),
            new StoreTypeSelection(
              asset: 'assets/resturant_icon.png',
              selected: selectedType==1,
              onTap: () {onTypeChosen(1);},
            ),
            new StoreTypeSelection(
              asset: 'assets/bar_icon.png',
              selected: selectedType==2,
              onTap: () {onTypeChosen(2);},
            ),
          ],
        ),
      ),
      elevation: 2.0,
    );
  }
}

class StoreTypeSelection extends StatelessWidget {
  final String asset;
  final bool selected;
  final VoidCallback onTap;
  StoreTypeSelection({this.asset, this.selected, this.onTap});
  @override
  Widget build(BuildContext context) {
    return new Expanded(
      flex: 1,
      child: new Container(
        padding: const EdgeInsets.all(12.0),
        child: new Material(
          color: selected?Colors.blue:Colors.white,
          child: new Padding(
            padding: const EdgeInsets.all(2.0),
            child: new InkWell(
              child: new Image.asset(
                asset,
                color: selected ? Colors.white: Colors.blue,
              ),
              onTap: onTap,
            ),
          ),
        ),
      ),
    );
  }
}
