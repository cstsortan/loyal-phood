import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

class SimpleLabel extends StatelessWidget {
  final String text;
  SimpleLabel({
    @required this.text,
  });
  @override
  Widget build(BuildContext context) {
    return new Container(
      padding: const EdgeInsets.all(5.0),
      child: new Align(
        alignment: Alignment.centerLeft,
        child: new Text(
          text,
          style: new TextStyle(
              fontWeight: FontWeight.bold,
              color: new Color(0xFF585858),
              fontSize: 8.0),
        ),
      ),
    );
  }
}
