import 'package:flutter/material.dart';
import 'bottom_card_info.dart';
import 'bouncy_card.dart';
import 'store_stack_photo.dart';

class NearbyStore extends StatelessWidget {
  final String storeName;
  final double rating;
  final int reviewsCount;
  final String foodType;
  final String deliveryInfo;
  final String distance;
  final String delay;
  final String photo;
  NearbyStore({
    this.storeName = 'Hacienda Grande',
    this.photo,
    this.rating = 4.2,
    this.reviewsCount = 1234,
    this.foodType = 'Burger, Klevis',
    this.deliveryInfo = 'Free Delivery',
    this.distance = '1.3 km',
    this.delay = '30-60 min',
  });

  @override
  Widget build(BuildContext context) {
    return new Container(
      height: 160.0,
      child: new BouncyCard(
        child: new Column(
            children: <Widget>[
              new Flexible(
                flex: 2,
                child: new StoreStackPhoto(
                  start: new NameAndRatings(
                    name: storeName,
                    rating: rating,
                    reviewsCount: reviewsCount,
                  ),
                  end: null,
                ),
              ),
              new Flexible(
                flex: 1,
                child: new Container(
                  margin:
                      new EdgeInsets.symmetric(horizontal: 8.0),
                  child: new BottomCardInfo(
                    bottomLeft: new DeliveryInfoLabel(
                      deliveryInfo: deliveryInfo,
                    ),
                    topLeft: new FoodTypeLabel(
                      foodType: foodType,
                    ),
                    topRight: new DistanceInfoLabel(
                      info: distance,
                    ),
                    bottomRight: new DistanceInfoLabel(
                      info: delay,
                    ),
                  ),
                ),
              ),
            ],
          ),
      ),
    );
  }
}
