import 'package:flutter/material.dart';

class BouncyCard extends StatefulWidget {
  final Widget child;
  BouncyCard({
    this.child,
  });
  @override
  _BouncyCardState createState() => new _BouncyCardState();
}

class _BouncyCardState extends State<BouncyCard>
    with SingleTickerProviderStateMixin {
  AnimationController _controller;
  Animation<double> animation;
  CurvedAnimation curve;

  @override
  void initState() {
    super.initState();
    _controller = new AnimationController(
      vsync: this,
      duration: const Duration(
        milliseconds: 150,
      ),
    );
    curve = new CurvedAnimation(
      curve: Curves.easeIn,
      parent: _controller,
    )
      ..addListener(() {
        setState(() {});
      })
      ..addStatusListener((status) {
        if (status == AnimationStatus.completed) {
          _controller.reverse();
        }
      });
    animation = new Tween(
      begin: 0.0,
      end: 10.0,
    )
        .animate(curve);
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return new Container(
      margin: new EdgeInsets.symmetric(
        horizontal: 5.0 + animation.value,
        vertical: 5.0 + animation.value,
      ),
      child: new Material(
        elevation: 2.0,
        shadowColor: Colors.grey,
        borderRadius: new BorderRadius.circular(10.0),
        type: MaterialType.card,
        child: new InkWell(
          onHighlightChanged: (changed) {
            if (changed) {
              _controller.forward();
            }
          },
          onTap: () {
            _controller.forward();
          },
          child: widget.child,
        ),
      ),
    );
  }
}
