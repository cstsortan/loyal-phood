import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

class Star extends StatelessWidget {
  final double fill;
  final double scale;
  Star({@required this.fill, this.scale = 1.0}):
    assert(fill<=1.0 && fill >=0.0);

  @override
  Widget build(BuildContext context) {
    return new Container(
      margin: new EdgeInsets.only(
        right: 2.5*scale,
      ),
      child: new Material(
        elevation: 2.0,
        borderRadius: new BorderRadius.circular(3.0),
        child: new Container(
          height: 14.0*scale,
          width: 14.0*scale,
          child: new Stack(
            children: <Widget>[
              new Container(
                color: Colors.blue,
                width: this.fill * 14.0*scale,
              ),
              new Center(
                child: new Material(
                  elevation: 3.0,
                  color: new Color(0x00000000),
                  child: new Icon(
                    Icons.star,
                    size: 12.0*scale,
                    color: Colors.white,
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
