import 'package:flutter/material.dart';

class TransparentSurface extends StatefulWidget {
  @override
  _TransparentSurfaceState createState() => new _TransparentSurfaceState();
}

class _TransparentSurfaceState extends State<TransparentSurface>
    with SingleTickerProviderStateMixin {
  AnimationController _controller;
  Tween colorTween = new ColorTween(
    begin: new Color(0x00000000),
    end: new Color(0xAA000000),
  );
  Animation<Color> animation;
  
  @override
  void initState() {
    super.initState();
    _controller = new AnimationController(
      vsync: this,
      duration: new Duration(
        milliseconds: 150,
      ),
    );
    animation = colorTween.animate(_controller)
    ..addListener((){
      setState((){});
    });
    _controller.forward();
  }
  
  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }
  
  @override
  Widget build(BuildContext context) {
    return new Container(
      color: animation.value
    );
  }
}