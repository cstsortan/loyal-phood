import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'dart:math';

class RotatedPlusFab extends StatefulWidget {
  final VoidCallback onTap;
  RotatedPlusFab({@required this.onTap});

  @override
  _RotatedPlusFabState createState() => new _RotatedPlusFabState();
}

class _RotatedPlusFabState extends State<RotatedPlusFab>
    with SingleTickerProviderStateMixin {
  AnimationController _controller;
  Tween tween = new Tween(begin: 0.0, end: 5 * PI / 4);
  Tween fabSizeTween = new Tween(
    begin: 70.0,
    end: 120.0,
  );
  Animation<double> animation;
  Animation<double> fabSizeAnimation;

  CurvedAnimation curve;
  bool _fabClicked;

  @override
  void initState() {
    super.initState();
    _controller = new AnimationController(
      vsync: this,
      duration: const Duration(
        milliseconds: 500,
      ),
    )..addListener(() {
        setState(() {});
      });
    curve = new CurvedAnimation(
      parent: _controller,
      curve: Curves.elasticOut,
      reverseCurve: Curves.elasticIn,
    );
    animation = tween.animate(curve);
    fabSizeAnimation = fabSizeTween.animate(curve);
    _fabClicked = false;
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    Widget _buildFab(int index) {
      return new Container(
        margin: new EdgeInsets.only(
          bottom: index==1 ? 38.0 : 0.0,
          left: 10.0,
          right: 10.0,
        ),
        child: _fabClicked
            ? new FloatingActionButton(
                child: new Icon(Icons.ac_unit),
                onPressed: () {},
              )
            : null,
      );
    }

    return new Column(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        new Row(
          verticalDirection: VerticalDirection.down,
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            _buildFab(0),
            _buildFab(1),
            _buildFab(2),
          ],
        ),
        new Transform.rotate(
          angle: animation.value,
          child: new Container(
            height: fabSizeAnimation.value,
            width: fabSizeAnimation.value,
            child: new Material(
              color: Colors.white,
              type: MaterialType.circle,
              child: new InkWell(
                child: new Container(
                  decoration: const BoxDecoration(
                    shape: BoxShape.circle,
                    color: Colors.blue,
                  ),
                  child: new Center(
                    child: new Text(
                      '+',
                      style: new TextStyle(color: Colors.white, fontSize: 58.0),
                    ),
                  ),
                ),
                onTap: () {
                  if (_controller.status == AnimationStatus.forward ||
                      _controller.status == AnimationStatus.reverse) {
                    return;
                  }

                  this.widget.onTap();

                  _controller.stop();
                  if (_fabClicked) {
                    _controller.reverse();
                  } else {
                    _controller.forward();
                  }

                  setState((){
                    _fabClicked = !_fabClicked;
                  });
                },
              ),
            ),
          ),
        ),
      ],
    );
  }
}
