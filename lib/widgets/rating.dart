import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

import 'star.dart';

class Rating extends StatefulWidget {
  final double rating;
  final double scale;

  Rating({@required this.rating, this.scale = 1.0})
      : assert(rating <= 5 && rating >= 0);

  @override
  _RatingState createState() => new _RatingState();
}

class _RatingState extends State<Rating> with SingleTickerProviderStateMixin {
  AnimationController _controller;
  CurvedAnimation curve;
  Animation<double> animation;

  @override
  void initState() {
    super.initState();
    _controller = new AnimationController(
      vsync: this,
      duration: new Duration(
        milliseconds: 400*widget.rating.toInt(),
      ),
    )..addListener(() {
        setState(() {});
        // print(animation.value);
      });
    curve = new CurvedAnimation(
      curve: Curves.linear,
      parent: _controller,
    );
    animation = new Tween(
      begin: 0.0,
      end: 1.0,
    )
        .animate(curve);
    _controller.forward();
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    // final int neededStars = _getStarsNeeded();
    final List<Star> stars = new List.generate(5, (index) {
      return new Star(
        fill: _getFill(index),
        scale: widget.scale, //*animation.value,
      );
    });

    return new Row(
      children: stars,
    );
  }

  int _getStarsNeeded() {
    double r = widget.rating * animation.value;
    if (r <= 0)
      return 0;
    else if (r > 0 && r <= 1)
      return 1;
    else if (r > 1 && r <= 2)
      return 2;
    else if (r > 2 && r <= 3)
      return 3;
    else if (r > 3 && r <= 4)
      return 4;
    else
      return 5;
  }

  double _getEndingStarFill() {
    return (widget.rating * animation.value + 1 - _getStarsNeeded());
  }

  double _getFill(index) {
    int neededStars = _getStarsNeeded();
    if (index < neededStars - 1) {
      return 1.0;
    } else if (index == neededStars - 1) {
      return _getEndingStarFill();
    } else
      return 0.0;
  }
}
