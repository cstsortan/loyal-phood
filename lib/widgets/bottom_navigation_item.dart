import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

class BottomNavigationItem extends StatelessWidget {
  final String name;
  final String assetIconName;
  final bool checked;
  final IconData icon;
  final VoidCallback onTap;
  BottomNavigationItem({
    this.assetIconName,
    this.icon,
    @required this.name,
    @required this.checked,
    @required this.onTap,
  });

  @override
  Widget build(BuildContext context) {
    return new Expanded(
      child: new Material(
        child: new InkWell(
          onTap: onTap,
          child: new Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              // new Image.asset(
              //   'assets/$assetIconName${checked?'_selected':''}.png',
              //   fit: BoxFit.scaleDown,
              // ),
              new Icon(
                icon,
                color: checked ? Colors.blue : Colors.grey,
                size: checked ? 26.0 : 24.0,
              ),
              new Text(
                name,
                style: new TextStyle(
                  color: checked ? Colors.blue : Colors.black54,
                  fontSize: checked ? 12.0 : 11.0,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
