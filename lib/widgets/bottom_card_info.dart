import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

class BottomCardInfo extends StatelessWidget {
  final Widget topLeft;
  final Widget topRight;
  final Widget bottomLeft;
  final Widget bottomRight;
  BottomCardInfo({
    this.topLeft,
    this.bottomLeft,
    this.bottomRight,
    this.topRight,
  });
  @override
  Widget build(BuildContext context) {
    return new Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        new Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            new Container(
              child: topLeft,
            ),
            new Container(
              child: bottomLeft,
            ),
          ],
        ),
        new Column(
          crossAxisAlignment: CrossAxisAlignment.end,
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            new Container(
              child: topRight,
            ),
            new Container(
              child: bottomRight,
            ),
          ],
        ),
      ],
    );
  }
}

class FoodTypeLabel extends StatelessWidget {
  final String foodType;
  final double scale;
  FoodTypeLabel({
    @required this.foodType,
    this.scale = 1.0,
  });
  @override
  Widget build(BuildContext context) {
    return new Text(
      foodType,
      style: new TextStyle(
        fontSize: 11.0*scale,
      ),
    );
  }
}

class DeliveryInfoLabel extends StatelessWidget {
  final String deliveryInfo;
  final double scale;
  DeliveryInfoLabel({
    @required this.deliveryInfo,
    this.scale = 1.0,
  });
  @override
  Widget build(BuildContext context) {
    return new Text(
      'Free Delivery',
      style: new TextStyle(color: new Color(0xFF53A925), fontSize: 10.0*scale),
    );
  }
}

class DistanceInfoLabel extends StatelessWidget {
  final String info;
  final double scale;
  DistanceInfoLabel({
    @required this.info,
    this.scale = 1.0,
  });

  @override
  Widget build(BuildContext context) {
    return new Text(
      "$info",
      style: new TextStyle(
        fontSize: 10.0*scale,
        color: Colors.grey,
      ),
    );
  }
}
