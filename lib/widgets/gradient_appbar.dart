import 'package:flutter/material.dart';

class GradientAppbar extends StatelessWidget implements PreferredSizeWidget {
  @override
  Widget build(BuildContext context) {
    final double statusBarHeight = MediaQuery.of(context).padding.top;
    return new Material(
      elevation: 2.0,
      child: new Container(
        child: new Row(
          children: <Widget>[
            new Container(
              margin: const EdgeInsets.only(
                left: 14.0
              ),
              child: new Image.asset('assets/settings_btn.png'),
            ),
            new Expanded(
              child: new Image.asset('assets/phood_logo.png'),
            ),
            new Container(
              margin: const EdgeInsets.only(
                right: 14.0
              ),
            ),
          ],
        ),
        padding: new EdgeInsets.only(top: statusBarHeight),
        height: preferredSize.height + statusBarHeight,
        decoration: new BoxDecoration(
          gradient: new LinearGradient(
            colors: [
              new Color(0xFF349CFE),
              new Color(0xFF237BCE),
            ],
            begin: Alignment.topCenter,
            end: Alignment.bottomCenter,
            tileMode: TileMode.clamp,
          ),
        ),
      ),
    );
  }

  @override
  Size get preferredSize => new Size.fromHeight(40.0);
}
