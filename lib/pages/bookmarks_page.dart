import 'package:flutter/material.dart';
import '../widgets/bookmark_store.dart';
import '../widgets/simple_label.dart';

class BookmarksPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new Container(
      padding: const EdgeInsets.all(0.0),
      child: new Column(
        children: <Widget>[
          new Padding(
            padding: const EdgeInsets.symmetric(
              horizontal: 5.0,
              vertical: 1.0,
            ),
            child: new SimpleLabel(
              text: 'BOOKMARKS',
            ),
          ),
          new Expanded(
            flex: 1,
            child: new ListView(
              padding: const EdgeInsets.all(0.0),
              children: new List.generate(3, (index){
                return new BookmarkStore();
              }),
            ),
          ),
        ],
      ),
    );
  }
}