import 'dart:async';

import 'package:flutter/material.dart';
import '../widgets/bottom_nav.dart';
import '../widgets/gradient_appbar.dart';
import '../widgets/rotated_plus_fab.dart';
import '../widgets/transparent_surface.dart';
import 'bookmarks_page.dart';
import 'check_ins_page.dart';
import 'nearby_stores_page.dart';
import 'profile_page.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_messaging/firebase_messaging.dart';

final FirebaseAuth _auth = FirebaseAuth.instance;
final Firestore _db = Firestore.instance;

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => new _HomePageState();
}

class _HomePageState extends State<HomePage> {
  bool _actionButtonPressed;
  int _pageChosen;

  PageController _pageController;

  @override
  void initState() {
    super.initState();

    _pageController = new PageController(
      initialPage: 0,
      keepPage: true,
    );
    _pageController.addListener(() {});
    _actionButtonPressed = false;
    _pageChosen = 0;
  }

  @override
  void dispose() {
    _pageController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    List<Widget> stackItems = [
      new Container(
        child: new Column(
          children: <Widget>[
            new GradientAppbar(),
            new Expanded(
              child: new PageView(
                controller: _pageController,
                onPageChanged: (int page) => setState(() {
                      _pageChosen = page;
                    }),
                children: <Widget>[
                  new NearbyStoresPage(),
                  new CheckInsPage(),
                  new BookmarksPage(),
                  new ProfilePage(),
                ],
              ),
            ),
            new BottomNav(_pageChosen, pageChosen),
          ],
        ),
      ),
      new Align(
        child: new RotatedPlusFab(
          onTap: () {
            setState(() {
              _actionButtonPressed = !_actionButtonPressed;
            });
          },
        ),
        alignment: const Alignment(0.0, 1.03),
      ),
    ];

    if (_actionButtonPressed) {
      stackItems.insert(1, new TransparentSurface());
    }

    return new Scaffold(
      backgroundColor: new Color(0xFFF0F0F0),
      body: new Stack(children: stackItems),
    );
  }

  void pageChosen(int page) {
    setState(() {
      _pageChosen = page;
    });
    _pageController.animateToPage(
      page,
      duration: const Duration(
        milliseconds: 500,
      ),
      curve: Curves.ease,
    );
  }
}
