import 'dart:async';

import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';

final FirebaseAuth _auth = FirebaseAuth.instance;

class ProfilePageX extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new Container(
      child: new Center(
        child: new Text('TEeeext'),

        // child: new StreamBuilder(
        //   stream: _auth.onAuthStateChanged,
        //   builder: (BuildContext context, AsyncSnapshot<FirebaseUser> s) {
        //     print('listened auth chang');
        //     FirebaseUser user = s.data;
        //     if (user == null) {
        //       return new Column(
        //         children: <Widget>[
        //           new Text("You're not signed in!"),
        //           new RaisedButton(
        //             child: new Text('Sign in'),
        //             onPressed: () {
        //               _auth.signInAnonymously();
        //             },
        //           ),
        //         ],
        //       );
        //     } else {
        //       return new Column(
        //         children: <Widget>[
        //           new Text(
        //             "You're signed in: ${user.uid}",
        //           ),
        //           new RaisedButton(
        //             child: new Text('Sign out'),
        //             onPressed: () {
        //               _auth.signOut();
        //             },
        //           ),
        //         ],
        //       );
        //     }
        //   },
        // ),
      ),
    );
  }
}

class ProfilePage extends StatefulWidget {
  @override
  _ProfilePageState createState() => new _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {
  FirebaseUser _user;
  StreamSubscription listener;

  @override
  void initState() {
    super.initState();
    listener = _auth.onAuthStateChanged.listen((FirebaseUser user) {
      print(user != null ? '${user.uid}' : 'not available!!');
      setState(() {
        _user = user;
      });
    });
  }

  @override
  void dispose() {
    super.dispose();
    listener.cancel();
  }

  @override
  Widget build(BuildContext context) {
    return new Container(
      child: new Column(
        children: <Widget>[
          new Text(_user != null ? '${_user.uid}' : 'not available!!'),
          new RaisedButton(
            child: new Text('Sign ${_user==null?'in':'out'}'),
            onPressed: (){
              if(_user==null) {
                _auth.signInAnonymously();
              } else {
                _auth.signOut();
              }
            },
          ),
        ],
      ),
    );
  }
}
