import 'package:flutter/material.dart';

import '../widgets/bottom_card_info.dart';
import '../widgets/bouncy_card.dart';
import '../widgets/simple_label.dart';
import '../widgets/store_stack_photo.dart';

class CheckInsPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new Container(
      child: new Column(
        children: <Widget>[
          new Padding(
            padding: const EdgeInsets.only(
              left: 5.0,
              top: 5.0,
            ),
            child: new SimpleLabel(
              text: 'CHECK-INS',
            ),
          ),
          new CheckInsForArea(),
          new Padding(
            padding: const EdgeInsets.only(
              left: 5.0,
              top: 5.0,
            ),
            child: new SimpleLabel(
              text: 'FRIENDS WITH SHARED CHECK-INS',
            ),
          ),
          new FriendsWithSharedCheckIns(),
        ],
      ),
    );
  }
}

class StoreCheckInCard extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new Material(
      elevation: 1.0,
      color: Colors.white,
      borderRadius: new BorderRadius.circular(5.0),
      child: new Column(
        children: <Widget>[
          new Flexible(
            flex: 2,
            child: new StoreStackPhoto(
              start: new NameAndRatings(
                name: 'Klevis Burger',
                rating: 5.0,
                reviewsCount: 65535,
                scale: 0.8,
              ),
            ),
            // child: new Container(),
          ),
          new Flexible(
            flex: 1,
            child: new Container(
              padding: const EdgeInsets.all(4.0),
              decoration: new BoxDecoration(),
              child: new BottomCardInfo(
                topLeft: new FoodTypeLabel(
                  foodType: 'Burger, Klevis',
                  scale: .7,
                ),
                bottomLeft: new FoodTypeLabel(
                  foodType: '\$\$\$',
                  scale: .7,
                ),
                topRight: new Align(
                  alignment: Alignment.topRight,
                  child: new DistanceInfoLabel(
                    info: '1.3 mi',
                    scale: .9,
                  ),
                ),
                bottomRight: new Text(' '),
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class CheckInsForArea extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new Container(
      height: 200.0,
      child: new BouncyCard(
        child: new Column(
          children: <Widget>[
            new Flexible(
              flex: 2,
              child: new Stack(
                children: <Widget>[
                  new Image.network(
                    'http://web-greece.gr/wp-content/uploads/2016/03/katerini_beach.jpg',
                    fit: BoxFit.fitWidth,
                    width: MediaQuery.of(context).size.width,
                  ),
                  new Padding(
                    padding: const EdgeInsets.only(
                      left: 16.0,
                    ),
                    child: new Align(
                      alignment: Alignment.centerLeft,
                      child: new Text(
                        'Paralia, Katerini',
                        style: new TextStyle(
                            color: Colors.white,
                            letterSpacing: 0.5,
                            fontSize: 16.0,
                            fontWeight: FontWeight.w900),
                      ),
                    ),
                  )
                ],
              ),
            ),
            new Flexible(
              flex: 5,
              child: new Container(
                child: new Row(
                  children: <Widget>[
                    new Flexible(
                      flex: 1,
                      child: new Container(
                        margin: const EdgeInsets.all(4.0),
                        child: new StoreCheckInCard(),
                      ),
                    ),
                    new Flexible(
                      flex: 1,
                      child: new Container(
                        margin: const EdgeInsets.all(4.0),
                        child: new StoreCheckInCard(),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class FriendsWithSharedCheckIns extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new Container(
      child: new Flexible(
        child: new ListView.builder(
          padding: const EdgeInsets.all(8.0),
          scrollDirection: Axis.horizontal,
          itemCount: 16,
          itemBuilder: (BuildContext context, int index) {
            return new FriendAvatar();
          },
        ),
      ),
    );
  }
}

class FriendAvatar extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new Padding(
      padding: const EdgeInsets.only(
        right: 8.0
      ),
      child: new Column(
        children: <Widget>[
          new CircleAvatar(
            backgroundColor: Colors.red,
            radius: 20.0,
          ),
          new SimpleLabel(
            text: 'Christos',
          ),
        ],
      ),
    );
  }
}
