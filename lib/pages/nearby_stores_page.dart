import 'package:flutter/material.dart';

import '../widgets/nearby_store.dart';
import '../widgets/simple_label.dart';
import '../widgets/types_list_card.dart';

class NearbyStoresPage extends StatefulWidget {
  @override
  _NearbyStoresPageState createState() => new _NearbyStoresPageState();
}

class _NearbyStoresPageState extends State<NearbyStoresPage> {

  int _selectedType;

  @override
  void initState() {
    super.initState();
    _selectedType = 1;
  }

  String _getType(int type) {
    switch(type) {
      case 0: return 'CAFE';
      case 1: return 'RESTAURANT';
      default: return 'BAR';
    }
  }

  @override
  Widget build(BuildContext context) {
    return new Column(
      children: <Widget>[
        new TypesListCard(
          selectedType: _selectedType,
          onTypeChosen: (int chosenType) {
            setState((){
              _selectedType = chosenType;
            });
          },
        ),
        new Expanded(
          child: new Padding(
            padding: const EdgeInsets.only(
              top: 5.0,
              left: 5.0,
              right: 5.0,
            ),
            child: new Column(
              children: <Widget>[
                new SimpleLabel(
                  text: 'NEARBY ${_getType(_selectedType)}',
                ),
                new Expanded(
                  child: new ListView.builder(
                    itemCount: 50,
                    padding: const EdgeInsets.only(bottom: 10.0),
                    itemBuilder: (BuildContext context, int index) {
                      return new NearbyStore();
                    },
                  ),
                ),
              ],
            ),
          ),
        ),
      ],
    );
  }
}
